package writer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import model.TreeEntry;
import model.TreeMode;
import model.TreeObject;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.jupiter.api.Test;
import reader.TreeReader;

class TreeWriterTest {

  private static final TreeEntry ENTRY_1 =
      new TreeEntry(TreeMode.DIRECTORY, "dir1", DigestUtils.sha1Hex("dir1"));
  private static final TreeEntry ENTRY_2 =
      new TreeEntry(TreeMode.DIRECTORY, "dir2", DigestUtils.sha1Hex("dir2"));
  private static final TreeEntry ENTRY_3 =
      new TreeEntry(TreeMode.REGULAR_FILE, "file1", DigestUtils.sha1Hex("file1"));
  private static final TreeObject TREE_OBJECT = new TreeObject(List.of(ENTRY_1, ENTRY_2, ENTRY_3));

  @Test
  void writeRead_1() throws IOException {
    var tmpPath = Files.createTempFile("TreeWriterTest_", "");
    var tmpFile = tmpPath.toFile();
    TreeWriter.write(TREE_OBJECT, tmpFile);

    var parsed = TreeReader.read(tmpFile);

    assertNotNull(parsed);
    assertEquals(3, parsed.getEntries().size());
    var first = parsed.getEntries().getFirst();
    assertEquals(ENTRY_1, first);
    var second = parsed.getEntries().get(1);
    assertEquals(ENTRY_2, second);
    var third = parsed.getEntries().get(2);
    assertEquals(ENTRY_3, third);
  }
}
