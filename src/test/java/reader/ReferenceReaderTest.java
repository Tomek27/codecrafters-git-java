package reader;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.nio.charset.StandardCharsets;

class ReferenceReaderTest {

  private static byte[] exampleData() {
    String a = """
        001e# service=git-upload-pack
        00000148f52672d6445ee4299fa8efca8bdfb59b6915f765 HEAD
        """;
    String b = """
        multi_ack thin-pack side-band side-band-64k ofs-delta shallow deepen-since deepen-not deepen-relative no-progress include-tag multi_ack_detailed allow-tip-sha1-in-want allow-reachable-sha1-in-want no-done symref=HEAD:refs/heads/master filter object-format=sha1 agent=git/2.45.1
        003ff52672d6445ee4299fa8efca8bdfb59b6915f765 refs/heads/master
        0000
        """;
    return b.getBytes(StandardCharsets.UTF_8);
  }

  @Test
  @DisplayName("GIVEN parse example WHEN parsed THEN expected object")
  void parse_1() {

  }
}
