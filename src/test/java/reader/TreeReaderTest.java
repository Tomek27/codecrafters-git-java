package reader;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.DeflaterOutputStream;
import model.TreeEntry;
import model.TreeMode;
import model.TreeObject;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TreeReaderTest {

  private static final byte NULL = 0;

  private static final String printContent =
      """
          040000 dir1 dc4cf8f1f15fe6599935bb5eaba462297f57243e
          040000 dir2 130c2beb3d67370ff19707ec13014bb6e1c46362
          100644 file1 60b27f004e454aca81b0480209cce5081ec52390""";

  private static final String printContentNameOnly = "dir1\ndir2\nfile1";

  private static byte[] bytesTreeObject() {
    ByteBuffer buf = ByteBuffer.allocate(166);
    buf.put("tree ".getBytes(StandardCharsets.UTF_8));
    buf.put("100".getBytes(StandardCharsets.UTF_8));
    buf.put(NULL);
    buf.put("040000 dir1".getBytes(StandardCharsets.UTF_8));
    buf.put(NULL);
    buf.put(DigestUtils.sha1("dir1"));
    buf.put("040000 dir2".getBytes(StandardCharsets.UTF_8));
    buf.put(NULL);
    buf.put(DigestUtils.sha1("dir2"));
    buf.put("100644 file1".getBytes(StandardCharsets.UTF_8));
    buf.put(NULL);
    buf.put(DigestUtils.sha1("file1"));
    return buf.array();
  }

  private static File writeTmpTreeFile() throws IOException {
    File file = File.createTempFile("TreeReaderTest_", "");
    file.deleteOnExit();
    try (var output = new DeflaterOutputStream(new FileOutputStream(file))) {
      byte[] data = bytesTreeObject();
      output.write(data);
      output.flush();
    }
    return file;
  }

  private static TreeObject treeObject() {
    var entries = new ArrayList<TreeEntry>();
    entries.add(new TreeEntry(TreeMode.DIRECTORY, "dir1", DigestUtils.sha1Hex("dir1")));
    entries.add(new TreeEntry(TreeMode.DIRECTORY, "dir2", DigestUtils.sha1Hex("dir2")));
    entries.add(new TreeEntry(TreeMode.REGULAR_FILE, "file1", DigestUtils.sha1Hex("file1")));
    return new TreeObject(entries);
  }

  private static boolean allEntriesEqual(List<TreeEntry> expected, List<TreeEntry> parsed) {
    if (expected.size() != parsed.size()) {
      throw new IllegalArgumentException("Both list have not the same size");
    }
    for (int i = 0; i < expected.size(); ++i) {
      if (!expected.get(i).equals(parsed.get(i))) {
        return false;
      }
    }
    return true;
  }

  @Test
  @DisplayName(
      """
          GIVEN example tree object file
          WHEN read from the file
          THEN the Tree Object is created
          """)
  void read_1() throws IOException {
    var treeFile = writeTmpTreeFile();

    var parsed = TreeReader.read(treeFile);

    assertNotNull(parsed);
    TreeObject treeObject = treeObject();
    assertTrue(allEntriesEqual(treeObject.getEntries(), parsed.getEntries()));
  }

  @Test
  @DisplayName(
      """
          GIVEN example tree object
          WHEN its content fetched
          THEN it will contain all entries with all info
          """)
  void printContent_1() {
    var treeObject = treeObject();

    var content = treeObject.getPrintContent();

    assertEquals(printContent, content);
  }

  @Test
  @DisplayName(
      """
          GIVEN example tree object
          WHEN its content but name only is fetched
          THEN it will contain just the names
          """)
  void printContent_2() {
    var treeObject = treeObject();

    var contentNameOnly = treeObject.getPrintContentNameOnly();

    assertEquals(printContentNameOnly, contentNameOnly);
  }
}
