package reader;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.zip.DeflaterOutputStream;
import model.BlobObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class BlobReaderTest {
  private static byte[] uncompressedBlob() {
    ByteBuffer buf = ByteBuffer.allocate(19);
    buf.put("blob ".getBytes(StandardCharsets.UTF_8));
    String content = "hello world";
    buf.put(Objects.toString(content.length()).getBytes(StandardCharsets.UTF_8));
    buf.put((byte) 0);
    buf.put(content.getBytes(StandardCharsets.UTF_8));
    return buf.array();
  }

  private static File writeTmpCompressedBlob() throws IOException {
    File file = File.createTempFile("BlobReaderTest_", "");
    file.deleteOnExit();
    try (var output = new DeflaterOutputStream(new FileOutputStream(file))) {
      byte[] blob = uncompressedBlob();
      output.write(blob);
      output.flush();
    }
    return file;
  }

  @Test
  @DisplayName(
      """
          GIVEN created tmp blob object file
          WHEN the file is read
          THEN the desired Blob Object is read
          """)
  void read_1() throws IOException {
    File blob = writeTmpCompressedBlob();

    var parsed = BlobReader.read(blob);

    assertNotNull(parsed);
    assertEquals(new BlobObject("hello world"), parsed);
  }
}
