package mapper;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import model.TreeEntry;
import model.TreeMode;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class ByteArrayMapperTest {

  private static byte[] expectedMapping() {
    ByteBuffer buf = ByteBuffer.allocate(11 + 20);
    buf.put("40000 dir1".getBytes(StandardCharsets.UTF_8));
    buf.put((byte) 0);
    var hash = DigestUtils.sha1("dir1");
    buf.put(hash);
    return buf.array();
  }

  @Test
  @DisplayName("Mapping of a given TreeEntry to byte arrays")
  void map_1() {
    var entry = new TreeEntry(TreeMode.DIRECTORY, "dir1", DigestUtils.sha1Hex("dir1"));

    var mapped = ByteArrayMapper.mapTreeEntry(entry);

    var expected = expectedMapping();
    assertArrayEquals(expected, mapped);
  }
}
