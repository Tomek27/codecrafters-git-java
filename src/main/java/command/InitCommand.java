package command;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import lombok.extern.slf4j.Slf4j;
import util.FileUtil;

@Slf4j
public class InitCommand {

  private InitCommand() {
  }

  public static void run(File projDir) {
    try {
      File objects = FileUtil.objectsDir(projDir);
      objects.mkdirs();
      File refs = FileUtil.refsDir(projDir);
      refs.mkdirs();
      File head = FileUtil.head(projDir);
      head.createNewFile();
      Files.write(head.toPath(), "ref: refs/heads/main\n".getBytes());
      log.info("Initialized git directory");
    } catch (IOException e) {
      log.error("Initializing git directory", e);
      throw new RuntimeException(e);
    }
  }
}
