package command.committree;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneOffset;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import model.CommitObject;
import util.FileUtil;
import writer.CommitWriter;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CommitTreeCommand {

  public static void run(String... args) throws IOException {
    var options = CommitTreeOptions.parse(args);
    var commit =
        CommitObject.builder()
            .treeHash(options.getTreeHash())
            .parentHash(options.getParentHash())
            .message(options.getMessage())
            .authorName("Joe Doe")
            .authorEmail("joe.doe@example.com")
            .authorInstant(Instant.now())
            .authorTimeZone(ZoneOffset.UTC)
            .committerName("Joe Doe")
            .committerEmail("joe.doe@example.com")
            .committerInstant(Instant.now())
            .committerTimeZone(ZoneOffset.UTC)
            .build();
    var hash = commit.getHash();
    var file = FileUtil.objectsFile(hash);
    CommitWriter.write(commit, file);
    System.out.println(hash);
  }
}
