package command.committree;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

@Slf4j
@Getter
@Builder
@AllArgsConstructor
public class CommitTreeOptions extends Options {
  private static final Option PARENT_HASH =
      Option.builder().option("p").hasArg().desc("parent's hash commit").build();
  private static final Option MESSAGE =
      Option.builder().option("m").hasArg().desc("commit message").build();

  private String treeHash;
  private String parentHash;
  private String message;

  private CommitTreeOptions() {
    addOption(PARENT_HASH);
    addOption(MESSAGE);
  }

  static CommitTreeOptions parse(String... args) {
    if (args.length < 3) {
      throw new IllegalArgumentException("Not enough parameters provided");
    }

    var opts = new CommitTreeOptions();

    try {
      var parser = new DefaultParser();
      var commandLine = parser.parse(opts, args);
      opts.parentHash = commandLine.getOptionValue(PARENT_HASH, "");
      if (opts.parentHash.isBlank()) {
        throw new IllegalArgumentException("parent hash cannot be blank");
      }
      opts.message = commandLine.getOptionValue(MESSAGE, "");
      if (opts.message.isBlank()) {
        throw new IllegalArgumentException("message cannot be blank");
      }
      opts.treeHash = commandLine.getArgList().get(1);
    } catch (ParseException e) {
      log.warn("Parse options:", e);
    }

    return opts;
  }
}
