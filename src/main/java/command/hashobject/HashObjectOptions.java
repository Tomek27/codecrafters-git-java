package command.hashobject;

import java.io.File;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

@Slf4j
@Getter
@Builder
@AllArgsConstructor
public class HashObjectOptions extends Options {
  private static final Option WRITE =
      Option.builder().option("w").desc("write blob to filesystem").build();

  private File file;
  private boolean writeFile;

  private HashObjectOptions() {
    addOption(WRITE);
  }

  static HashObjectOptions parse(String... args) {
    if (args.length < 2) {
      throw new IllegalArgumentException("Not enough arguments provided");
    }

    var opts = new HashObjectOptions();

    try {
      var parser = new DefaultParser();
      var commandLine = parser.parse(opts, args);
      opts.file = new File(commandLine.getArgList().get(1));
      opts.writeFile = commandLine.hasOption(WRITE);
    } catch (ParseException e) {
      log.warn("Parse options:", e);
    }

    return opts;
  }
}
