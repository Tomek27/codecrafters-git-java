package command.hashobject;

import java.io.File;
import java.io.IOException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import model.BlobObject;
import reader.BlobReader;
import util.FileUtil;
import writer.BlobWriter;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class HashObjectCommand {

  public static void run(String... args) throws IOException {
    var options = HashObjectOptions.parse(args);
    if (options.isWriteFile()) {
      writeBlobAndPrintHash(options);
    } else {
      printHash(options);
    }
  }

  public static String writeBlob(HashObjectOptions options) throws IOException {
    File file = options.getFile();
    String content = FileUtil.readContent(file);
    BlobObject blob = new BlobObject(content);
    String hash = blob.getHash();
    File blobFile = FileUtil.objectsFile(hash);
    BlobWriter.write(blob, blobFile);
    return hash;
  }

  static void printHash(HashObjectOptions options) throws IOException {
    File file = options.getFile();
    BlobObject blob = BlobReader.read(file);
    System.out.println(blob.getHash());
  }

  private static void writeBlobAndPrintHash(HashObjectOptions options) throws IOException {
    String hash = writeBlob(options);
    System.out.println(hash);
  }
}
