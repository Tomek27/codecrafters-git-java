package command.lstree;

import java.io.File;
import java.io.IOException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import model.TreeObject;
import reader.TreeReader;
import util.FileUtil;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LsTreeCommand {

  public static void run(String... args) throws IOException {
    var options = LsTreeOptions.parse(args);
    File treeFile = FileUtil.objectsFile(options.getTreeHash());
    TreeObject treeObject = TreeReader.read(treeFile);
    if (options.isNameOnly()) {
      System.out.println(treeObject.getPrintContentNameOnly());
    } else {
      System.out.println(treeObject.getPrintContent());
    }
  }
}
