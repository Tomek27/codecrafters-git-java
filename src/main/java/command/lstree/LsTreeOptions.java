package command.lstree;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

@Slf4j
@Getter
class LsTreeOptions extends Options {

  private static final Option NAME_ONLY =
      Option.builder().longOpt("name-only").desc("List only name of the tree objects").build();

  private String treeHash;
  private boolean nameOnly;

  private LsTreeOptions() {
    addOption(NAME_ONLY);
  }

  public static LsTreeOptions parse(String... args) {
    var options = new LsTreeOptions();

    try {
      var parser = new DefaultParser();
      var commandLine = parser.parse(options, args);
      options.treeHash = commandLine.getArgList().get(1);
      options.nameOnly = commandLine.hasOption(NAME_ONLY);
    } catch (ParseException e) {
      log.warn("Parse failed", e);
    }

    return options;
  }
}
