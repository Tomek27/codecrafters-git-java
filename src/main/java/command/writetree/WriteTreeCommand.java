package command.writetree;

import command.hashobject.HashObjectCommand;
import command.hashobject.HashObjectOptions;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import model.TreeEntry;
import model.TreeMode;
import model.TreeObject;
import util.FileUtil;
import writer.TreeWriter;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class WriteTreeCommand {

  private static final Set<String> IGNORE_DIRS = Set.of(".git");
  private static final Set<String> IGNORE_FILES = Set.of(".gitignore");

  private final List<TreeEntry> entries = new ArrayList<>();

  public static void run(File workingDir) throws IOException {
    var cmd = new WriteTreeCommand();
    var hash = cmd.handleTree(workingDir);
    System.out.println(hash);
  }

  private static String writeTree(TreeObject treeObject) throws IOException {
    String hash = treeObject.getHash();
    File treeFile = FileUtil.objectsFile(hash);
    TreeWriter.write(treeObject, treeFile);
    return hash;
  }

  private void handleBlob(File file) throws IOException {
    var options = HashObjectOptions.builder().file(file).writeFile(true).build();
    var hash = HashObjectCommand.writeBlob(options);
    var entry = new TreeEntry(TreeMode.REGULAR_FILE, file.getName(), hash);
    entries.add(entry);
  }

  private String handleTree(File dir) throws IOException {
    try (var children = Files.list(dir.toPath())) {
      children
          .filter(child -> !IGNORE_DIRS.contains(child.toFile().getName()))
          .filter(child -> !IGNORE_FILES.contains(child.toFile().getName()))
          .forEach(
              child -> {
                try {
                  File childFile = child.toFile();
                  if (childFile.isFile()) {
                    handleBlob(childFile);
                  } else {
                    var cmd = new WriteTreeCommand();
                    var hash = cmd.handleTree(childFile);
                    var entry = new TreeEntry(TreeMode.DIRECTORY, childFile.getName(), hash);
                    entries.add(entry);
                  }
                } catch (IOException e) {
                  throw new RuntimeException(e);
                }
              });
    }

    var treeObj = new TreeObject(entries);
    var hash = writeTree(treeObj);
    var entry = new TreeEntry(TreeMode.DIRECTORY, dir.getName(), hash);
    entries.add(entry);
    return hash;
  }
}
