package command.clone;

import java.io.File;
import java.net.URI;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
class CloneOptions {

  private URI cloneUri;
  private File targetDir;

  static CloneOptions parse(String... args) {
    if (args.length < 3) {
      throw new IllegalArgumentException("Not enough arguments provided");
    }

    var opts = new CloneOptions();
    opts.cloneUri = URI.create(args[1]);
    opts.targetDir = new File(args[2]);
    return opts;
  }
}
