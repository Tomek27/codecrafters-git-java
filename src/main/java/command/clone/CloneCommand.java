package command.clone;

import client.GitClient;
import client.Reference;
import command.InitCommand;
import java.util.List;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CloneCommand {

  public static void run(String... args) {
    var options = CloneOptions.parse(args);
    System.out.println("Clone: " + options.getCloneUri());
    System.out.println("Target: " + options.getTargetDir());
    InitCommand.run(options.getTargetDir());

    GitClient client = GitClient.withBaseUri(options.getCloneUri());
    List<Reference> references = client.fetchReferences();
    System.out.println("References:");
    references.forEach(System.out::println);
  }
}
