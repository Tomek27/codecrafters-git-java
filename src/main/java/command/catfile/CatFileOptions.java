package command.catfile;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

@Slf4j
@Getter
public class CatFileOptions extends Options {
  public static final Option PATH =
      Option.builder("p").desc("hash of the blob file to read").required().hasArg().build();

  private String blobHash;

  private CatFileOptions() {
    addOption(PATH);
  }

  public static CatFileOptions parse(String[] args) {
    var opts = new CatFileOptions();

    try {
      var parser = new DefaultParser();
      var commandLine = parser.parse(new CatFileOptions(), args);
      if (commandLine.hasOption(PATH)) {
        opts.blobHash = commandLine.getOptionValue(PATH);
      }
    } catch (ParseException e) {
      log.warn("Parsing options: ", e);
    }

    return opts;
  }
}
