package command.catfile;

import java.io.File;
import java.io.IOException;
import model.BlobObject;
import reader.BlobReader;
import util.FileUtil;

public class CatFileCommand {

  public static void run(String... args) throws IOException {
    var options = CatFileOptions.parse(args);

    String sha = options.getBlobHash();
    File blobFile = FileUtil.objectsFile(sha);
    BlobObject blobObject = BlobReader.read(blobFile);
    System.out.print(blobObject.getContent());
  }
}
