package util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class HexUtil {

  public static byte[] fromStringToBytes(String hash) {
    try {
      return Hex.decodeHex(hash);
    } catch (DecoderException e) {
      throw new RuntimeException(e);
    }
  }
}
