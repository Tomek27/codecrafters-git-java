package util;

import java.time.Instant;
import java.time.ZoneOffset;
import java.util.Objects;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class StringUtil {

  public static String fromInstant(Instant instant) {
    return Objects.toString(instant.toEpochMilli() / 1000);
  }

  public static String fromZoneOffset(ZoneOffset zoneOffset) {
    return zoneOffset.toString().replace(":", "");
  }
}
