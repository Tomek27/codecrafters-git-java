package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FileUtil {
  public static File rootDir(File projDir) {
    return new File(projDir, ".git");
  }

  public static File refsDir(File projDir) {
    return new File(rootDir(projDir), "refs");
  }

  public static File head(File projDir) {
    return new File(rootDir(projDir), "HEAD");
  }

  public static File objectsDir(File projDir) {
    return new File(rootDir(projDir), "objects");
  }

  public static File objectsFile(String sha) {
    File folder = new File(objectsDir(workingDir()), sha.substring(0, 2));
    return new File(folder, sha.substring(2));
  }

  public static String readContent(File file) throws IOException {
    try (var input = new FileInputStream(file)) {
      byte[] data = input.readAllBytes();
      return new String(data, StandardCharsets.UTF_8);
    }
  }

  public static File workingDir() {
    return new File("");
  }
}
