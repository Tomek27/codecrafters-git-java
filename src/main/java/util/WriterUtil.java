package util;

import java.io.File;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class WriterUtil {

  public static void tryCreateParentDirsAndFile(File file) {
    tryCreateParentDirectories(file);
    tryCreateFile(file);
  }

  private static void tryCreateParentDirectories(File file) {
    try {
      if (!file.getParentFile().mkdirs()) {
        log.warn("Has not created parent directories for file: {}", file);
      }
    } catch (Exception ex) {
      log.warn("Couldn't create parent directories for file: {}", file, ex);
    }
  }

  private static void tryCreateFile(File file) {
    try {
      if (!file.createNewFile()) {
        log.warn("Has not created git/object file: {}", file);
      }
    } catch (Exception ex) {
      log.warn("Couldn't create git/object file: {}", file, ex);
    }
  }
}
