package mapper;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import model.TreeEntry;
import util.HexUtil;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ByteArrayMapper {

  public static final int HASH_BYTES = 20;

  public static byte[] mapTreeEntry(TreeEntry entry) {
    String mode = Objects.toString(entry.treeMode().getValue());
    int length = mode.length() + 1 + entry.name().length() + 1 + HASH_BYTES;
    ByteBuffer buf = ByteBuffer.allocate(length);
    buf.put(mode.getBytes(StandardCharsets.UTF_8));
    buf.put(" ".getBytes(StandardCharsets.UTF_8));
    buf.put(entry.name().getBytes(StandardCharsets.UTF_8));
    buf.put((byte) 0);
    buf.put(HexUtil.fromStringToBytes(entry.hash()));
    return buf.array();
  }
}
