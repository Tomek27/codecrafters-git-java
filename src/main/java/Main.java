import command.InitCommand;
import command.catfile.CatFileCommand;
import command.clone.CloneCommand;
import command.committree.CommitTreeCommand;
import command.hashobject.HashObjectCommand;
import command.lstree.LsTreeCommand;
import command.writetree.WriteTreeCommand;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import util.FileUtil;


@Slf4j
public class Main {

  public static void main(String[] args) throws IOException {
    final String command = args[0];

    switch (command) {
      case "init" -> InitCommand.run(FileUtil.workingDir());
      case "cat-file" -> CatFileCommand.run(args);
      case "clone" -> CloneCommand.run(args);
      case "commit-tree" -> CommitTreeCommand.run(args);
      case "hash-object" -> HashObjectCommand.run(args);
      case "ls-tree" -> LsTreeCommand.run(args);
      case "write-tree" -> WriteTreeCommand.run(FileUtil.workingDir());
      default -> log.info("Unknown command: {}", command);
    }
  }
}
