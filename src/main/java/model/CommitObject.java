package model;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import util.StringUtil;

@Builder
@AllArgsConstructor
public class CommitObject extends FileObject {

  // Fri May 22 18:14:29 2009 -0700
  private static final DateTimeFormatter DATE_TIME_FORMATTER =
      DateTimeFormatter.ofPattern("E M dd HH:mm:ss yyyy");

  private String authorName;
  private String authorEmail;
  private Instant authorInstant;
  private ZoneOffset authorTimeZone;

  private String committerName;
  private String committerEmail;
  private Instant committerInstant;
  private ZoneOffset committerTimeZone;

  private String treeHash;
  private String parentHash;
  private String message;

  @Override
  public String getType() {
    return "commit";
  }

  @Override
  public int getContentLength() {
    return getCommitDataBytes().length;
  }
  
  @Override
  public byte[] getFileBytes() {
    var commitData = getCommitDataBytes();
    var header = "commit " + commitData.length + "\0";
    var headerData = header.getBytes(StandardCharsets.UTF_8);
    ByteBuffer buf = ByteBuffer.allocate(headerData.length + commitData.length);
    buf.put(headerData);
    buf.put(commitData);
    return buf.array();
  }

  private byte[] getCommitDataBytes() {
    return getCommitData().getBytes(StandardCharsets.UTF_8);
  }

  private String getCommitData() {
    var commitData = "tree " + treeHash + "\n";
    if (parentHash != null) {
      commitData += "parent " + parentHash + "\n";
    }
    commitData +=
        "author "
            + authorName
            + " <"
            + authorEmail
            + "> "
            + StringUtil.fromInstant(authorInstant)
            + " "
            + StringUtil.fromZoneOffset(authorTimeZone)
            + "\n";
    commitData +=
        "committer "
            + committerName
            + " <"
            + committerEmail
            + "> "
            + StringUtil.fromInstant(committerInstant)
            + " "
            + StringUtil.fromZoneOffset(committerTimeZone)
            + "\n";
    commitData += "\n" + message + "\n";
    return commitData;
  }
}
