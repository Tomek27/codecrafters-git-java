package model;

import lombok.Getter;

@Getter
public enum TreeMode {
  REGULAR_FILE(100644),
  EXECUTABLE_FILE(100755),
  SYMBOLIC_LINK(120000),
  DIRECTORY(40000);

  private final int value;

  TreeMode(int value) {
    this.value = value;
  }

  public static TreeMode fromString(String s) {
    int value = Integer.parseInt(s);
    for (TreeMode mode : TreeMode.values()) {
      if (mode.value == value) {
        return mode;
      }
    }
    throw new IllegalArgumentException("Unknown Tree Mode: " + s);
  }
}
