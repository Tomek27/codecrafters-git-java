package model;

import org.apache.commons.codec.digest.DigestUtils;

public abstract class FileObject {

  public abstract String getType();

  public abstract int getContentLength();

  public abstract byte[] getFileBytes();

  public String getHash() {
    return DigestUtils.sha1Hex(getFileBytes());
  }

  public String getHeader() {
    return getType() + ' ' + getContentLength() + "\0";
  }
}
