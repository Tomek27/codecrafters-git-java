package model;


public record TreeEntry(TreeMode treeMode, String name, String hash) {

  public String getContent() {
    return String.format("%06d %s %s", treeMode.getValue(), name, hash);
  }
}
