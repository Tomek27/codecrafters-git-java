package model;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import mapper.ByteArrayMapper;

@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Getter
public class TreeObject extends FileObject {

  private final List<TreeEntry> entries;

  @Override
  public String getType() {
    return "tree";
  }

  @Override
  public int getContentLength() {
    return getPrintContent().length();
  }
  
  @Override
  public byte[] getFileBytes() {
    var entriesBytes = getEntriesBytes();
    var entriesBytesLength = entriesBytes.stream().map(arr -> arr.length).reduce(0, Integer::sum);
    var header = getType() + ' ' + entriesBytesLength + '\0';
    var headerBytes = header.getBytes(StandardCharsets.UTF_8);
    ByteBuffer buf = ByteBuffer.allocate(headerBytes.length + entriesBytesLength);
    buf.put(headerBytes);
    entriesBytes.forEach(buf::put);
    return buf.array();
  }

  public String getPrintContent() {
    return getEntriesSorted().map(TreeEntry::getContent).collect(Collectors.joining("\n"));
  }

  public String getPrintContentNameOnly() {
    return getEntriesSorted().map(TreeEntry::name).collect(Collectors.joining("\n"));
  }

  private Stream<TreeEntry> getEntriesSorted() {
    return entries.stream().sorted(Comparator.comparing(TreeEntry::name));
  }

  private List<byte[]> getEntriesBytes() {
    return getEntriesSorted()
        .sorted(Comparator.comparing(TreeEntry::name))
        .map(ByteArrayMapper::mapTreeEntry)
        .toList();
  }
}
