package model;

import java.nio.charset.StandardCharsets;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class BlobObject extends FileObject {

  private final String content;

  @Override
  public String getType() {
    return "blob";
  }

  @Override
  public byte[] getFileBytes() {
    String fileContent = getHeader() + content;
    return fileContent.getBytes(StandardCharsets.UTF_8);
  }

  @Override
  public int getContentLength() {
    return content.length();
  }
}
