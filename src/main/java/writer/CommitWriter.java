package writer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.DeflaterOutputStream;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import model.CommitObject;
import util.WriterUtil;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CommitWriter {

  public static void write(CommitObject obj, File file) throws IOException {
    WriterUtil.tryCreateParentDirsAndFile(file);
    try (var fos = new FileOutputStream(file)) {
      write(obj, fos);
    }
  }

  private static void write(CommitObject obj, OutputStream output) throws IOException {
    try (var zip = new DeflaterOutputStream(output)) {
      zip.write(obj.getFileBytes());
      zip.flush();
    }
  }
}
