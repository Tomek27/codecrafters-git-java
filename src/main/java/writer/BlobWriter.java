package writer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.DeflaterOutputStream;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import model.BlobObject;
import util.WriterUtil;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BlobWriter {

  public static void write(BlobObject obj, File file) throws IOException {
    WriterUtil.tryCreateParentDirsAndFile(file);
    try (var fos = new FileOutputStream(file)) {
      write(obj, fos);
    }
  }

  private static void write(BlobObject obj, OutputStream output) throws IOException {
    try (var zip = new DeflaterOutputStream(output)) {
      zip.write(obj.getFileBytes());
      zip.flush();
    }
  }
}
