package reader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.zip.InflaterInputStream;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import model.BlobObject;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BlobReader {

  public static BlobObject read(File file) throws IOException {
    try (var fs = new FileInputStream(file)) {
      return read(fs);
    }
  }

  static BlobObject read(InputStream input) throws IOException {
    try (var zip = new InflaterInputStream(input)) {
      byte[] fileContent = zip.readAllBytes();
      int idxContent = ReaderUtil.findIdxContent(fileContent);
      int size = fileContent.length - idxContent;
      byte[] contentBytes = new byte[size];
      System.arraycopy(fileContent, idxContent, contentBytes, 0, size);
      String content = new String(contentBytes, StandardCharsets.UTF_8);
      return new BlobObject(content);
    }
  }
}
