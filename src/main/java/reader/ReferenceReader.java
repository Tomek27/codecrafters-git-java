package reader;

import client.Reference;
import java.util.ArrayList;
import java.util.List;

public class ReferenceReader {

  private final List<Reference> list = new ArrayList<>();
  private final byte[] data;

  private ReferenceReader(byte[] data) {
    this.data = data;
  }

  public static List<Reference> parse(byte[] data) {
    ReferenceReader reader = new ReferenceReader(data);
    return reader.list;
  }
}
