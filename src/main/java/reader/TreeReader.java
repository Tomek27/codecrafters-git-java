package reader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.InflaterInputStream;
import model.TreeEntry;
import model.TreeMode;
import model.TreeObject;
import org.apache.commons.codec.binary.Hex;

public class TreeReader {
  private static final int HASH_BYTES = 20;
  private final List<TreeEntry> entries = new ArrayList<>();
  private final byte[] data;
  private int offset = 0;

  private TreeReader(byte[] data) {
    this.data = data;
  }

  public static TreeObject read(File file) throws IOException {
    try (var fs = new FileInputStream(file)) {
      return read(fs);
    }
  }

  static TreeObject read(InputStream input) throws IOException {
    try (var zip = new InflaterInputStream(input)) {
      byte[] fileContent = zip.readAllBytes();
      TreeReader reader = new TreeReader(fileContent);
      reader.parse();
      return new TreeObject(reader.entries);
    }
  }

  private void parse() {
    offset = ReaderUtil.findIdxContent(data);
    while (offset < data.length && data[offset] != 0) {
      parseEntry();
    }
  }
  
  private void parseEntry() {
    TreeMode mode = parseMode();
    String name = parseName();
    String hash = parseHash();

    var entry = new TreeEntry(mode, name, hash);
    entries.add(entry);
  }

  private TreeMode parseMode() {
    var string = new StringBuilder();
    while (data[offset] != ' ') {
      string.append((char) data[offset]);
      offset++;
    }
    offset++;
    return TreeMode.fromString(string.toString());
  }

  private String parseName() {
    var string = new StringBuilder();
    while (data[offset] != 0) {
      string.append((char) data[offset]);
      offset++;
    }
    offset++;
    return string.toString();
  }

  private String parseHash() {
    byte[] hashData = new byte[HASH_BYTES];
    System.arraycopy(data, offset, hashData, 0, HASH_BYTES);
    String hash = Hex.encodeHexString(hashData);
    offset = offset + HASH_BYTES;
    return hash;
  }
}
