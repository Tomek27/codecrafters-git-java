package reader;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
class ReaderUtil {
  static int findIdxContent(byte[] data) {
    int idx = findNextNull(data);
    if (idx == -1) {
      throw new IllegalArgumentException("Index of content not found");
    }
    return idx + 1;
  }

  private static int findNextNull(byte[] data) {
    for (int i = 0; i < data.length; ++i) {
      if (data[i] == 0) {
        return i;
      }
    }
    return -1;
  }
}
