package client;

public record Reference(String name, String hash) {
}
