package client;

import feign.Feign;
import feign.Headers;
import feign.QueryMap;
import feign.RequestLine;
import reader.ReferenceReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.List;
import java.util.Map;

public interface GitClient {

  @RequestLine("GET /info/refs")
  byte[] fetchReferences(@QueryMap Map<String, Object> queryMap);

  default List<Reference> fetchReferences() {
    byte[] response = fetchReferences(Map.of("service", "git-upload-pack"));
    return ReferenceReader.parse(response);
  }

  static GitClient withBaseUri(URI baseUri) {
    try {
      return withBaseUrl(baseUri.toURL());
    } catch (MalformedURLException e) {
      throw new RuntimeException(e);
    }
  }

  static GitClient withBaseUrl(URL baseUrl) {
    return Feign.builder()
        .target(GitClient.class, baseUrl.toString());
  }
}
